AWS CLI version --- 2.7.12
Helm version -- v3.9.0
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update
kubectl create namespace istio-system
helm install istio-base istio/base -n istio-system --version 1.14.1
helm install istiod istio/istiod -n istio-system --wait --version 1.14.1
kubectl create namespace dare
kubectl label namespace dare istio-injection=enabled
helm install instio-ingress istio/gateway -n dare --wait --version 1.14.1

kubectl config set-context --current --namespace=dare

Note: - https://istio.io/latest/docs/ops/deployment/requirements/
Jaeger consideration for tracing. There is a header that must be included in applications for tracing to work


# Deploy the BookInfo application
export MYHOST=$(kubectl config view -o jsonpath={.contexts..namespace}).bookinfo.com

# Deploy the application 
kubectl apply -l version!=v2,version!=v3 -f https://raw.githubusercontent.com/istio/istio/release-1.14/samples/bookinfo/platform/kube/bookinfo.yaml


# Troubleshooting Istio 
https://github.com/istio/istio/wiki/Troubleshooting-Istio#diagnostics

kubectl get mutatingwebhookconfiguration istio-sidecar-injector -o yaml -o jsonpath='{.webhooks[0].clientConfig.caBundle}' | md5sum
kubectl -n istio-system get configmap istio-ca-root-cert -o jsonpath='{.data.root-cert\.pem}' | base64 | md5sum


kubectl -n istio-system patch deployment istiod \
    -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"


kubectl get --raw /api/v1/namespaces/istio-system/services/https:istiod:https-webhook/proxy/inject -v4
I0618 07:39:46.663871   36880 helpers.go:216] server response object: [{
  "metadata": {},
  "status": "Failure",
  "message": "the server rejected our request for an unknown reason",
  "reason": "BadRequest",
  "details": {
    "causes": [
      {
        "reason": "UnexpectedServerResponse",
        "message": "no body found"
      }
    ]
  },
  "code": 400
}]
F0618 07:39:46.663940   36880 helpers.go:115] Error from server (BadRequest): the server rejected our request for an unknown reason










