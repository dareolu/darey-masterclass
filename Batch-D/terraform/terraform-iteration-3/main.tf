provider "aws" {
  region = "us-east-1"
}

terraform {
  required_version = ">= 1.1.7"
  backend "s3" {
    bucket = "devops-masterclass-state"
    key    = "terraformstate/terraform.tfstate"
    region = "eu-west-2"
  }
}

module "networks" {
  source   = "./modules/networking"
  vpc_cidr = var.vpc_cidr_from_root
  public_subnet = var.public_subnet
}


// module "security_groups" {
//   source   = "./modules/security_group"
//   vpc_id = module.networks.vpc_id
// }


output "vpc_id" {
  value = module.networks.vpc_id
}



# provider "aws" {
#   region = "us-east-1"
# }

# terraform {
#   required_version = ">= 1.1.7"
#   backend "s3" {
#     bucket = "devops-masterclass"
#     key    = "terraformstate/terraform.tfstate"
#     region = "eu-west-2"
#   }
# }

# module "vpc" {
#   source = "terraform-aws-modules/vpc/aws"
#   version = "3.16.0"
#   name = "my-vpc"
#   cidr = "10.0.0.0/16"

#   azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
#   private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
#   public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

#   enable_nat_gateway = true
#   enable_vpn_gateway = true

#   tags = {
#     Terraform = "true"
#     Environment = "dev"
#   }
# }

# // module "security_groups" {
# //   source   = "./modules/security_group"
# //   vpc_id = module.networks.vpc_id
# // }


# output "vpc_id" {
#   value = module.networks.vpc_id
# }