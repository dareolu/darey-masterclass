1. DevOps Career - The confusion with Solutions Architecture
2. Designing an AWS Infrastructure 
3. Manual implementation 
4. Infrastructure as code (Terraform)
5. Migration to Containerisation
6. Building Kubernetes Cluster from Ground up
7. Working with Kubernetes (From Beginner to Advanced)
    - Deploying applications into Kubernetes
    - Working with Helm
    - Ingress Controllers
    - Continous Integration with Jenkins (Jenkins Configuration As Code)
    - Helm and Kustomize
    - Continous Delivery with ArgoCD
    - Release strategies
8. Start looking for work as either a Solutions Architect or DevOps engineer